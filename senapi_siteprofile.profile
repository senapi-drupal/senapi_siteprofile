<?php

/**
 * @file
 * Enables modules and site configuration for a demo_umami site installation.
 */

use Drupal\contact\Entity\ContactForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function senapi_siteprofile_form_install_configure_form_alter(&$form, FormStateInterface $form_state) {

  // Site name.
  $form['site_information']['site_name']['#default_value'] = 'SENAPI';

  //Site email
  $form['site_information']['site_mail']['#default_value'] = 'plataforma@senapi.gob.bo';

  //user administrator
  $form['admin_account']['account']['name']['#default_value'] = 'webmaster';
  $form['admin_account']['account']['name']['#attributes']['disabled'] = TRUE;
  $form['admin_account']['account']['mail']['#default_value'] = 'sistemas@senapi.gob.bo';
  $form['admin_account']['account']['mail']['#description'] = '';

  //regional
  $form['regional_settings']['site_default_country']['#default_value'] = 'BO';
  $form['regional_settings']['date_default_timezone']['#default_value'] = 'America/La_Paz';

  //update
  $form['update_notifications']['enable_update_status_module']['#default_value'] = 0;
  $form['update_notifications']['enable_update_status_email']['#default_value'] = 0;

  $form['#submit'][] = 'senapi_siteprofile_form_install_configure_submit';
}

/**
 * Submission handler to sync the contact.form.feedback recipient.
 */
function senapi_siteprofile_form_install_configure_submit($form, FormStateInterface $form_state) {
  $site_mail = $form_state->getValue('site_mail');
  ContactForm::load('feedback')->setRecipients([$site_mail])->trustData()->save();
}

