<?php

namespace Drupal\senapi_content;

use Drupal\Component\Utility\Html;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Path\AliasManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a helper class for importing default content.
 */
class InstallHelper implements ContainerInjectionInterface {

  /**
   * The path alias manager.
   *
   * @var \Drupal\Core\Path\AliasManagerInterface
   */
  protected $aliasManager;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * State.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Import helper.
   *
   * @var \Drupal\senapi_content\ImportHelper
   */
  protected $entityHelper;

  /**
   * Constructs a new InstallHelper object.
   *
   * @param \Drupal\Core\Path\AliasManagerInterface $aliasManager
   *   The path alias manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   Module handler.
   * @param \Drupal\Core\State\StateInterface $state
   *   State service.
   * @param  \Drupal\senapi_content\ImportHelper $entityHelper
   *  Import service
   */
  public function __construct(
    AliasManagerInterface $aliasManager,
    EntityTypeManagerInterface $entityTypeManager,
    ModuleHandlerInterface $moduleHandler,
    StateInterface $state,
    ImportHelper $entityHelper) {
    $this->aliasManager = $aliasManager;
    $this->entityTypeManager = $entityTypeManager;
    $this->moduleHandler = $moduleHandler;
    $this->state = $state;
    $this->entityHelper = $entityHelper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('path.alias_manager'),
      $container->get('entity_type.manager'),
      $container->get('module_handler'),
      $container->get('state'),
      $container->get('senapi_content.import_helper')
    );
  }

  /**
   * Imports default contents.
   */
  public function importContent() {
    $this
      ->importEditors()
      ->importBlocks()
      ->importContentTypes()
      ->importGenerateUrl()
      ->importMenus();
  }

  /**
   * Import User editor author.
   *
   * @return $this
   */
  protected function importEditors() {
    $user_storage = $this->entityTypeManager->getStorage('user');

    $editors = [
      'Auditoria Interna',
      'Sistemas',
      'Oposiciones',
      'Patentes',
    ];

    foreach ($editors as $editor) {
      $user = $user_storage->create([
        'pass' => 'password',
        'name' => $editor,
        'status' => 1,
        'roles' => ['editor'],
        'mail' => mb_strtolower(str_replace(' ', '.', $editor)) . '@senapi.gob.bo',
      ]);
      $user->enforceIsNew();
      $user->save();
      $this->storeCreatedContentUuids([$user->uuid() => 'user']);
    }
    return $this;
  }


  /**
   * Imports Blocks.
   *
   * @return $this
   */
  protected function importBlocks() {
    $blockNames = [
      'basic_block',
    ];

    foreach ($blockNames as $blockName) {
      $csvFile = $this->resolveContentPath("/block/${$blockName}.csv");
      $this->importBlockContent($csvFile);
    }

    return $this;
  }

  /**
   * Import Block Custom.
   *
   * @return $this
   */
  protected function importBlockContent($csv_file) {

    if (!file_exists($csv_file)) {
      return $this;
    }

    $handle = $this->entityHelper->getFileHandler($csv_file);

    $header = $this->entityHelper->getCsvData($handle);
    $result = 0;
    $operations = [];
    while (($data = $this->entityHelper->getCsvData($handle)) !== FALSE) {
      $data = array_combine($header, $data);
      $rowData = [
        'data' => $data,
        'result' => ++$result,
      ];

      $operations[] = [
        '\Drupal\senapi_content\ImportHelper::importBlockFromCsv',
        [$rowData],
      ];
    }

    if (count($operations)) {
      $batch = [
        'title' => t('Importando CSV...'),
        'operations' => $operations,
        'finished' => '\Drupal\senapi_content\ImportHelper::importFromCsvFinishedCallback',
        'error_message' => t('La importación ha encontrado un error.'),
        'progress_message' => t('Importado @current de @total fila.'),
      ];
      batch_set($batch);
      fclose($handle);
    }

    return $this;
  }

  /**
   * Imports Content Types.
   *
   * @return $this
   */
  protected function importContentTypes() {
    $contentTypes = [
      'mainslider',
      'page',
      'prensa',
      'gaceta',
      'oposicion',
      'auditoriainterna',
      'materialdifusion',
      'memoriaanual',
      'revistapatente',
      'tecnologica',
      'media',
      'boletin',
      'regional',
      'produccion'
    ];

    foreach ($contentTypes as $contentType) {
      $zipFile = $this->resolveContentPath("node/$contentType/${contentType}.zip");
      $csvFile = $this->resolveContentPath("node/$contentType/${contentType}.csv");
      $this->importNode($contentType, $csvFile, $zipFile);
    }

    return $this;
  }

  /**
   * Imports Menus.
   *
   * @return $this
   */
  protected function importMenus() {
    $menuNames = [
      'menu_main',
      'menu_menucentera',
    ];

    foreach ($menuNames as $menuName) {
      $csvFile = $this->resolveContentPath("/menu/${menuName}.csv");
      $this->importMenu($csvFile);
    }

    return $this;
  }

  /**
   * Import Node Content Type.
   *
   * @param $content_type
   * @param $csv_file
   * @param null $zip_file
   *
   * @return $this
   */
  protected function importNode($content_type, $csv_file, $zip_file = NULL) {
    if (!file_exists($csv_file)) {
      return $this;
    }
    $uuids = [];

    $fields = $this->entityHelper->getFields($content_type);
    $fieldNames = $fields['name'];
    $fieldProperties = $fields['properties'];

    $csvFile = realpath($csv_file);

    $dirExtract = $this->entityHelper->folders['import_media']['relative'];
    if (file_exists($zip_file)) {
      $zipFile = realpath($zip_file);
      $archiver = $this->entityHelper->getArchiver($zipFile);
      $archiver->extractTo($this->entityHelper->folders['import_media']['relative']);

      $dirExtract = $this->entityHelper->folders['import_media']['relative'] . '/' . $content_type;
      if (!is_dir($dirExtract)) {
        $dirExtract = $this->entityHelper->folders['import_media']['relative'];
      }
    }

    $handle = $this->entityHelper->getFileHandler($csvFile);

    $header = $this->entityHelper->getCsvData($handle);

    $keyIndex = [];
    foreach ($header as $dataKey => $dataValue) {
      $dataValueOrigin = $dataValue;
      $row = explode('_', $dataValue);
      unset($row[0]);

      $dataValue = join("_", $row);

      if (in_array($dataValue, $fieldNames)) {

        $keyIndex[$dataValue]['key'] = $dataKey;
        $keyIndex[$dataValue]['properties'] = $fieldProperties[array_search($dataValue, $fieldNames)];

        if (isset($keyIndex[$dataValue]['properties']['setting']) && isset($keyIndex[$dataValue]['properties']['setting']['file_directory'])) {
          $destination = $this->entityHelper->getUploadLocation($keyIndex[$dataValue]['properties']['setting']);
          $this->entityHelper->rmdir($destination);
        }

        if (array_search($dataValueOrigin . '_alt', $header)) {
          $keyIndex[$dataValue]['alt'] = [
            'key' => array_search($dataValueOrigin . '_alt', $header),
          ];
        }

        if (array_search($dataValueOrigin . '_title', $header)) {
          $keyIndex[$dataValue]['title'] = [
            'key' => array_search($dataValueOrigin . '_title', $header),
          ];
        }

        if (array_search($dataValueOrigin . '_description', $header)) {
          $keyIndex[$dataValue]['description'] = [
            'key' => array_search($dataValueOrigin . '_description', $header),
          ];
        }

      }
      else {
        if ($dataValue == 'body_description' && in_array('body', $fieldNames)) {
          $bodyKey = array_search('body', $fieldNames);
          $keyIndex['body']['key'] = $dataKey;
          $keyIndex['body']['properties'] = $fieldProperties[$bodyKey];

          $keyIndex['body']['value'] = ['key' => $dataKey];
        }
        elseif ($dataValue == 'body_summary' && in_array('body', $fieldNames)) {
          $bodyKey = array_search('body', $fieldNames);
          $keyIndex['body']['key'] = $dataKey;
          $keyIndex['body']['properties'] = $fieldProperties[$bodyKey];

          $keyIndex['body']['summary'] = ['key' => $dataKey];
        }
      }
    }

    $nodes = [];
    $nodeArray = [];

    $dataTimeNow = new \Datetime('now');

    while ($data = $this->entityHelper->getCsvData($handle)) {

      foreach ($keyIndex as $itemKey => $itemValue) {
        if ($data[$itemValue['key']]) {
          $type = $itemValue['properties']['type'];
          if (in_array($content_type, ['gaceta', 'oposicion'])) {
            \Drupal::logger('senapi_content')
              ->info('Content Type: @content_type  import csv  header: @header, type: @type, key: @key, value: @message', [
                '@content_type' => $content_type,
                '@header' => $itemKey,
                '@type' => $type,
                '@key' => $itemValue['key'],
                '@message' => $data[$itemValue['key']],
              ]);
          }
          switch ($type) {
            case 'image':
              $imageSrc = $data[$itemValue['key']];
              $imageAlt = $nodeArray['title'];
              $imageTitle = $nodeArray['title'];

              if (isset($itemValue['alt']) && isset($itemValue['alt']['key']) && !empty($data[$itemValue['alt']['key']])) {
                $imageAlt = Html::escape($data[$itemValue['alt']['key']]);
              }

              if (isset($itemValue['title']) && isset($itemValue['title']['key']) && !empty($data[$itemValue['title']['key']])) {
                $imageTitle = Html::escape($data[$itemValue['title']['key']]);
              }

              $images = $this->entityHelper->uploadImageMedia($imageSrc, $imageAlt, $imageTitle, $dirExtract, $itemValue['properties']['setting']);

              if ($images) {
                $nodeArray[$itemKey] = $images;
              }
              break;

            case 'file':
              $fileDescription = $nodeArray['title'];
              if (isset($itemValue['description']) && isset($itemValue['description']['key']) && !empty($data[$itemValue['description']['key']])) {
                $fileDescription = Html::escape($data[$itemValue['description']['key']]);
              }

              $fileSrc = $data[$itemValue['key']];

              switch ($itemKey) {
                case 'field_gaceta_patente_file':
                case 'field_gaceta_signo_file':
                case 'field_gaceta_dautor_file':
                  if ($itemKey == 'field_gaceta_patente_file') {
                    switch ($fileDescription) {
                      case 'TOMO I':
                      case 'TOMO II':
                      case 'TOMO III':
                        $name = 'Tomo';
                        break;
                      default:
                        $name = 'Patentes';
                        break;
                    }
                  }

                  if ($itemKey == 'field_gaceta_signo_file') {
                    switch ($fileDescription) {
                      case 'TOMO I':
                      case 'TOMO II':
                      case 'TOMO III':
                        $name = 'Tomo';
                        break;
                      default:
                        $name = 'Signos';
                        break;
                    }
                  }
                  if ($itemKey == 'field_gaceta_dautor_file') {
                    switch ($fileDescription) {
                      case 'TOMO I':
                      case 'TOMO II':
                      case 'TOMO III':
                        $name = 'Tomo';
                        break;
                      default:
                        $name = 'Dautor';
                        break;
                    }
                  }
                  $date = new \DateTime($data[$keyIndex['field_gaceta_date']['key']]);
                  $attr = [
                    'name' => $name,
                    'number' => $data[$keyIndex['field_gaceta_number']['key']],
                    'date' => $date->format('dmY'),
                    'extension' => 'pdf',
                  ];
                  $files = $this->entityHelper->uploadFileMediaCustomName($fileSrc, $fileDescription, $dirExtract, $itemValue['properties']['setting'], $attr);
                  break;

                case 'field_oposicion_signo_file':
                case 'field_oposicion_patente_file':
                  if ($itemKey == 'field_oposicion_patente_file') {
                    $name = 'OP_Patentes';
                  }

                  if ($itemKey == 'field_oposicion_signo_file') {
                    $name = 'OP_Signos';
                  }

                  $title = $data[$keyIndex['field_oposicion_gaceta']['key']];

                  $node = \Drupal::entityQuery('node')
                    ->condition('type', 'gaceta')
                    ->condition('title', $title, 'IN')
                    ->execute();

                  if ($node) {
                    $node = Node::load(key($node));
                    $gaceta = $node->get('field_gaceta_number')
                      ->first()
                      ->getValue()['value'];
                    $gacetaDate = $node->get('field_gaceta_date')
                      ->first()
                      ->getValue()['value'];

                    $date = new \DateTime($gacetaDate);
                    $attr = [
                      'name' => $name,
                      'number' => $gaceta,
                      'date' => $date->format('dmY'),
                      'extension' => 'pdf',
                    ];
                    $files = $this->entityHelper->uploadFileMediaCustomName($fileSrc, $fileDescription, $dirExtract, $itemValue['properties']['setting'], $attr);
                  }
                  break;

                default:
                  $files = $this->entityHelper->uploadFileMedia($fileSrc, $fileDescription, $dirExtract, $itemValue['properties']['setting']);
                  break;
              }

              if ($files) {
                $nodeArray[$itemKey] = $files;
              }
              break;

            case 'entity_reference':

              if ($itemValue['properties']['setting']['target_type'] == 'taxonomy_term') {
                $term = $data[$itemValue['key']];
                $voc = key($itemValue['properties']['setting']['handler_settings']['target_bundles']);
                if ($voc) {
                  $terms = $this->entityHelper->getTermReference($voc, $term);
                  if ($terms) {
                    $nodeArray[$itemKey] = $terms;
                  }
                }
              }

              if ($itemValue['properties']['setting']['target_type'] == 'node') {
                $title = $data[$itemValue['key']];

                $node = \Drupal::entityQuery('node')
                  ->condition('type', 'gaceta')
                  ->condition('title', $title, 'IN')
                  ->execute();
                if ($node) {
                  $nodeArray[$itemKey][] = ['target_id' => key($node)];
                }
              }
              break;

            case 'datetime':
              $dataArrayItems = explode('|', $data[$itemValue['key']]);
              foreach ($dataArrayItems as $dataArrayItem) {
                $dateArray = explode(':', $dataArrayItem);
                if (count($dateArray) > 1) {
                  $dateTimeStamp = strtotime($dataArrayItem);
                  $newDateString = date('Y-m-d\TH:i:s', $dateTimeStamp);
                }
                else {
                  $dateTimeStamp = strtotime($dataArrayItem);
                  $newDateString = date('Y-m-d', $dateTimeStamp);
                }
                if (in_array($content_type, [
                    'gaceta',
                    'oposicion',
                  ]) && count($dateArray) < 1) {
                  $newDateString .= 'T04:00:00';
                }
                $nodeArray[$itemKey][] = ["value" => $newDateString];
              }
              break;

            case 'text_long':
              $summary = $data[$itemValue['key']];
              $nodeArray[$itemKey] = [
                'value' => $summary,
                'format' => 'full_html',
              ];
              break;

            case 'text_with_summary':
              if ($itemKey == 'body') {

                $value = $summary = "";

                if (isset($itemValue['summary']) && isset($itemValue['summary']['key'])) {
                  $summary = $data[$itemValue['summary']['key']];
                }

                if (isset($itemValue['value']) && isset($itemValue['value']['key'])) {
                  $value = $data[$itemValue['value']['key']];
                }

                $nodeArray['body'] = [
                  'value' => $value,
                  'summary' => $summary,
                  'format' => 'full_html',
                ];
              }
              break;

            case 'integer':
              if ($itemKey != 'nid') {
                $nodeArray[$itemKey] = $data[$itemValue['key']];
              }
              break;

            case 'created':
              break;

            case 'status':
              $nodeArray[$itemKey] = (int) $data[$itemValue['key']];
              break;

            case 'string':
              $nodeArray[$itemKey] = Html::escape($data[$itemValue['key']]);
              break;

            case 'list_string':
              $nodeArray[$itemKey] = $data[$itemValue['key']];
              break;

            case 'entity_reference_revisions':
              /* TODO pendiente */
              break;

            case 'boolean':
              $nodeArray[$itemKey] = (boolean) $data[$itemValue['key']];
              break;

            case 'link':
            case 'geolocation':
            case 'layout_section':
              $nodeArray[$itemKey] = unserialize($data[$itemValue['key']]);
              break;

            default:
              echo "default $type : $itemKey " . $data[$itemValue['key']];
              echo "<br>";
              break;
          }
        }
      }

      $nodeArray['type'] = $content_type;
      $nodeArray['moderation_state'] = 'published';

      $nodes[] = $nodeArray;

      $node = Node::create($nodeArray);

      $dataTime = new \Datetime();
      $dataTime->setTimestamp($dataTimeNow->getTimestamp());
      $dataTime->modify('+1 minutes');

      $node->setCreatedTime($dataTime->getTimestamp());
      $node->setChangedTime($dataTime->getTimestamp());

      $dataTimeNow = $dataTime;

      $node->setOwnerId($this->getUser('Comunicacion'));

      $node->status = 1;

      $node->save();
      $uuids[$node->uuid()] = 'node';
      unset($node);

      $nodeArray = [];
    }
    $this->storeCreatedContentUuids($uuids);

    if (is_dir($this->entityHelper->folders['import_media']['absolute'] . '/' . $content_type)) {
      $this->entityHelper->rmdir($this->entityHelper->folders['import_media']['absolute'] . '/' . $content_type);
    }

    return $this;
  }

  /**
   * Import menu.
   *
   * @return $this
   */
  protected function importMenu($csv_file) {

    if (!file_exists($csv_file)) {
      return $this;
    }

    $handle = $this->entityHelper->getFileHandler($csv_file);

    $header = $this->entityHelper->getCsvData($handle);
    $result = 0;
    $operations = [];
    while (($data = $this->entityHelper->getCsvData($handle)) !== FALSE) {
      $data = array_combine($header, $data);
      $rowData = [
        'data' => $data,
        'result' => ++$result,
      ];

      $operations[] = [
        '\Drupal\senapi_content\ImportHelper::importMenuFromCsv',
        [$rowData],
      ];
    }

    if (count($operations)) {
      $batch = [
        'title' => t('Importando CSV...'),
        'operations' => $operations,
        'finished' => '\Drupal\senapi_content\ImportHelper::importFromCsvFinishedCallback',
        'error_message' => t('La importación ha encontrado un error.'),
        'progress_message' => t('Importado @current de @total fila.'),
      ];
      batch_set($batch);
      fclose($handle);
    }
    return $this;
  }

  /**
   * Generate url
   *
   * @return $this
   */
  protected function importGenerateUrl() {
    $batch = [
      'title' => t('Bulk updating URL aliases'),
      'operations' => [
        [
          'Drupal\pathauto\Form\PathautoBulkUpdateForm::batchStart',
          [],
        ],
        [
          'Drupal\pathauto\Form\PathautoBulkUpdateForm::batchProcess',
          ['canonical_entities:node', 'create'],
        ],
      ],
      'finished' => 'Drupal\pathauto\Form\PathautoBulkUpdateForm::batchFinished',
    ];

    batch_set($batch);

    return $this;
  }

  /**
   * Resolve content path for import
   *
   * @param $file_path
   *
   * @return string|null
   */
  protected function resolveContentPath($file_path) {
    $modulePath = $this->moduleHandler->getModule('senapi_content')
      ->getPath();

    $internal = (!empty($file_path)) ? realpath($modulePath . '/content' . $file_path) : NULL;
    if (file_exists($internal)) {
      \Drupal::logger('senapi_content')
        ->info('Internal content @message', ['@message' => $internal]);
      return $internal;
    }

    $external = (!empty($file_path)) ? realpath(DRUPAL_ROOT . '/../../content' . $file_path) : NULL;
    if (file_exists($external)) {
      \Drupal::logger('senapi_content')
        ->info('External content @message', ['@message' => $external]);
      return $external;
    }

    $external = (!empty($file_path)) ? realpath('/media/datos/content' . $file_path) : NULL;
    if (file_exists($external)) {
      \Drupal::logger('senapi_content')
        ->info('External content @message', ['@message' => $external]);
      return $external;
    }

    return NULL;
  }

  /**
   * Stores record of content entities created by this import.
   *
   * @param array $uuids
   *   Array of UUIDs where the key is the UUID and the value is the entity
   *   type.
   */
  protected function storeCreatedContentUuids(array $uuids) {
    $uuids = $this->state->get('senapi_content_uuids', []) + $uuids;
    $this->state->set('senapi_content_uuids', $uuids);
  }

  /**
   * Get User, not found create.
   *
   * @param $name
   *
   * @return int|string|null
   */
  protected function getUser($name) {
    $user_storage = $this->entityTypeManager->getStorage('user');
    $users = $user_storage->loadByProperties(['name' => $name]);
    if (empty($users)) {
      // Creating user without any password.
      $user = $user_storage->create([
        'name' => $name,
        'pass' => 'password',
        'status' => 1,
        'roles' => ['author'],
        'mail' => mb_strtolower(str_replace(' ', '.', $name)) . '@senapi.gob.bo',
      ]);
      $user->enforceIsNew();
      $user->save();
      $this->storeCreatedContentUuids([$user->uuid() => 'user']);
      return $user->id();
    }
    $user = reset($users);
    return $user->id();
  }

  /**
   * Deletes any content imported by this module.
   *
   * @return $this
   */
  public function deleteImportedContent() {
    $uuids = $this->state->get('senapi_content_uuids', []);
    $by_entity_type = array_reduce(array_keys($uuids), function ($carry, $uuid) use ($uuids) {
      $entity_type_id = $uuids[$uuid];
      $carry[$entity_type_id][] = $uuid;
      return $carry;
    }, []);
    foreach ($by_entity_type as $entity_type_id => $entity_uuids) {
      $storage = $this->entityTypeManager->getStorage($entity_type_id);
      $entities = $storage->loadByProperties(['uuid' => $entity_uuids]);
      $storage->delete($entities);
    }
    return $this;
  }

  /**
   * Creates a file entity based on an image path.
   *
   * @param string $path
   *   Image path.
   *
   * @return int
   *   File ID.
   */
  protected function createFileEntity($path) {
    $uri = $this->fileUnmanagedCopy($path);
    $file = $this->entityTypeManager->getStorage('file')->create([
      'uri' => $uri,
      'status' => 1,
    ]);
    $file->save();
    $this->storeCreatedContentUuids([$file->uuid() => 'file']);
    return $file->id();
  }

  /**
   * Wrapper around file_unmanaged_copy().
   *
   * @param string $path
   *   Path to image.
   *
   * @return string|false
   *   The path to the new file, or FALSE in the event of an error.
   */
  protected function fileUnmanagedCopy($path) {
    $filename = basename($path);
    return file_unmanaged_copy($path, 'public://' . $filename, FILE_EXISTS_REPLACE);
  }
}
