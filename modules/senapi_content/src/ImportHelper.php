<?php

namespace Drupal\senapi_content;

use Drupal\Component\Render\PlainTextOutput;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\File\FileSystem;
use Drupal\Core\Utility\Token;
use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\node\Entity\NodeType;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Entity\Vocabulary;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class ImportHelper
 *
 * @package Drupal\senapi_content
 */
class ImportHelper {

  /**
   * Initialization directory.
   *
   * @var array
   */
  public $folders;

  /**
   * Drupal Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Drupal File System.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * Drupal query factory.
   *
   * @var \Drupal\Core\Entity\Query\QueryFactory
   */
  protected $entityQuery;

  /**
   * Drupal entity field manager
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $fieldManager;

  /**
   * Token.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * Constructor for \Drupal\image_export_import\EntitySaveHelper class.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity type manager.
   * @param \Drupal\Core\File\FileSystem $file_system
   *   The Form Builder.
   * @param \Drupal\Core\Entity\Query\QueryFactory $entityQuery
   *   The Query Builder.
   * @param \Drupal\Core\Entity\EntityFieldManager $fieldManager
   *   The Field manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, FileSystem $file_system, QueryFactory $entityQuery, EntityFieldManager $fieldManager, Token $token) {
    $this->entityTypeManager = $entity_type_manager;
    $this->fileSystem = $file_system;
    $this->entityQuery = $entityQuery;
    $this->fieldManager = $fieldManager;
    $this->token = $token;
    $this->folders = $this->createFolderExportImport();
  }

  /**
   * Initialization of directories for export and import.
   *
   * @return array
   */
  public function createFolderExportImport() {
    $srcPublic = "public://migrate";
    $srcMigrate = $this->fileSystem->realpath($srcPublic);

    $folders = [
      "public_migrate" => ['relative' => $srcPublic, 'absolute' => $srcMigrate],
      "export_data" => [
        'relative' => $srcPublic . "/export-data",
        'absolute' => $srcMigrate . '/export-data',
      ],
      "export_media" => [
        'relative' => $srcPublic . "/export-media",
        'absolute' => $srcMigrate . '/export-media',
      ],
      "import_data" => [
        'relative' => $srcPublic . "/import-data",
        'absolute' => $srcMigrate . '/import-data',
      ],
      "import_media" => [
        'relative' => $srcPublic . "/import-media",
        'absolute' => $srcMigrate . '/import-media',
      ],
    ];

    if (!is_dir($folders['public_migrate']['absolute'])) {
      mkdir($folders['public_migrate']['absolute'], 0777, TRUE);
      chmod($folders['public_migrate']['absolute'], 0777);
    }

    if (!is_dir($folders['export_data']['absolute'])) {
      mkdir($folders['export_data']['absolute'], 0777, TRUE);
      chmod($folders['export_data']['absolute'], 0777);
    }

    if (!is_dir($folders['export_media']['absolute'])) {
      mkdir($folders['export_media']['absolute'], 0777, TRUE);
      chmod($folders['export_media']['absolute'], 0777);
    }

    if (!is_dir($folders['import_data']['absolute'])) {
      mkdir($folders['import_data']['absolute'], 0777, TRUE);
      chmod($folders['import_data']['absolute'], 0777);
    }

    if (!is_dir($folders['import_media']['absolute'])) {
      mkdir($folders['import_media']['absolute'], 0777, TRUE);
      chmod($folders['import_media']['absolute'], 0777);
    }

    return $folders;
  }

  /**
   * Export Type of Content csv file.
   *
   * @param $filename
   * @param $content_types
   * @param $fields
   *
   * @return string
   */
  public function createCsvFileExportData($filename, $content_types, $fields) {
    $pathAbsolute = $this->folders['public_migrate']['absolute'] . "/$filename";
    $pathRelative = $this->folders['public_migrate']['relative'] . "/$filename";

    file_unmanaged_delete($pathAbsolute);

    $folder = $this->folders['export_media']['absolute'] . "/$content_types";

    if (!is_dir($folder)) {
      mkdir($folder, 0777, TRUE);
      chmod($folder, 0777);
    }

    $nids = \Drupal::entityQuery('node')
      ->condition('type', $content_types)
      ->execute();
    $nodes = $this->entityTypeManager->getStorage('node')->loadMultiple($nids);

    $file = fopen($pathAbsolute, 'w');

    $keyA = 0;
    foreach ($nodes as $node) {
      if ($keyA == 0) {
        $column = [];
        $column[0] = $content_types . '_nid';
        $column[1] = $content_types . '_title';
        $column[2] = $content_types . '_created';
        $column[3] = $content_types . '_created_datetime';

        foreach ($fields as $field) {
          $type = $node->getTypedData()
            ->getDataDefinition()
            ->getPropertyDefinition($field)
            ->getType();
          switch ($type) {
            case 'image':
              $column[] = $content_types . '_' . $field;
              $column[] = $content_types . '_' . $field . '_alt';
              $column[] = $content_types . '_' . $field . '_title';
              break;
            case 'file':
              $column[] = $content_types . '_' . $field;
              $column[] = $content_types . '_' . $field . '_description';
              break;
            case 'text_with_summary':
              $column[] = $content_types . '_' . $field . '_summary';
              $column[] = $content_types . '_' . $field . '_description';
              break;
            default:
              $column[] = $content_types . '_' . $field;
              break;
          }
        }
        fputcsv($file, $column);
      }

      $row = [];
      $row[0] = $node->get('nid')->value;
      $row[1] = $node->get('title')->value;
      $row[2] = $node->get('created')->value;
      $row[3] = date('Y-m-d H:i:s', $node->get('created')->value);

      foreach ($fields as $field) {
        $item_index = 0;
        $img_title = $img_alt = $file_description = $basename = [];

        $type = $node->getTypedData()
          ->getDataDefinition()
          ->getPropertyDefinition($field)
          ->getType();
        $setting = $node->getTypedData()
          ->getDataDefinition()
          ->getPropertyDefinition($field)
          ->getSettings();
        switch ($type) {
          case 'image':
          case 'file':
            if (!empty($node->get($field)->target_id) && ($values = $node->get($field)
                ->getValue()) && count($values) >= 1) {

              foreach ($node->get($field)->referencedEntities() as $item) {
                switch ($item->getEntityTypeId()) {
                  case 'file':
                    $data = $this->entityTypeManager->getStorage('file')
                      ->load($item->id());

                    $item_ = $values[$item_index];

                    if (!empty($data)) {
                      $basename[$item_index] = basename($data->getFileUri());
                      $src = $this->fileSystem->realpath($data->getFileUri());
                      $dest = $folder;

                      shell_exec("cp -r $src $dest");
                    }

                    switch ($type) {
                      case 'image':
                        $img_alt[$item_index] = ($item_['alt']) ? $item_['alt'] : '';
                        $img_title[$item_index] = ($item_['title']) ? $item_['title'] : '';
                        $item_index++;

                        if (count($values) == $item_index) {
                          $row[] = implode("|", $basename);
                          $row[] = implode("|", $img_alt);
                          $row[] = implode("|", $img_title);
                        }
                        break;

                      case 'file':
                        $file_description[$item_index] = ($item_['description']) ? $item_['description'] : '';
                        $item_index++;
                        if (count($values) == $item_index) {
                          $row[] = implode("|", $basename);
                          $row[] = implode("|", $file_description);
                        }
                        break;
                    }

                    break;
                }
              }
            }
            else {

              switch ($type) {
                case 'image':
                  $row[] = "";
                  $row[] = "";
                  $row[] = "";
                  break;
                case 'file':
                  $row[] = "";
                  $row[] = "";
                  break;
              }
            }
            break;

          case 'entity_reference':
            if (!empty($node->get($field)->target_id)) {
              $valuesReference = $node->get($field)->getValue();
              foreach ($node->get($field)->referencedEntities() as $item) {
                switch ($item->getEntityTypeId()) {
                  case 'taxonomy_term':
                    $data = $this->entityTypeManager->getStorage('taxonomy_term')
                      ->load($item->id());

                    if ($data) {
                      $basename[$item_index] = $data->getName();
                    }

                    $item_index++;

                    if (count($valuesReference) == $item_index) {
                      $row[] = implode("|", $basename);
                    }
                    break;
                  case 'node':
                    $data = $this->entityTypeManager->getStorage('node')
                      ->load($item->id());
                    if ($data) {
                      $row[] = $data->getTitle();
                    }
                    $item_index++;
                    break;
                }
              }
            }
            else {
              $row[] = "";
            }
            break;

          case 'list_string':
          case 'text_long':
            $row[] = $node->get($field)->value;
            break;

          case 'text_with_summary':
            $row[] = ($node->get($field)->summary) ? $node->get($field)->summary : '';
            $row[] = ($node->get($field)->value) ? $node->get($field)->value : '';

            break;

          case 'datetime':
            $values = $node->get($field)->getValue();
            if (count($values) > 1) {
              $val = [];
              foreach ($values as $value) {
                $val[] = $value['value'];
              }

              $row[] = implode("|", $val);
            }
            else {
              $row[] = $node->get($field)->value;
            }
            break;

          case 'path':
            $row[] = $node->get($field)->alias;
            break;

          case 'geolocation':
          case 'link':
          case 'layout_section':
            $row[] = serialize($node->get($field)->getValue());
            break;

          case 'status':
          case 'boolean':
          case 'string':
            $row[] = $node->get($field)->value;
            break;

          default:
            $row[] = $node->get($field)->value;
            break;
        }
      }

      $keyA++;

      fputcsv($file, $row);
    }


    fclose($file);

    return $pathRelative;
  }

  /**
   * Export Menu in csv file.
   *
   * @param $filename
   * @param $fields
   *
   * @return string
   */
  public function createCsvFileExportMenuData($filename, $fields) {
    $pathAbsolute = $this->folders['public_migrate']['absolute'] . "/$filename";
    $pathRelative = $this->folders['public_migrate']['relative'] . "/$filename";

    file_unmanaged_delete($pathAbsolute);

    $file = fopen($pathAbsolute, 'w');

    $column = [];
    $column[] = 'id';
    $column[] = 'uuid';
    $column[] = 'langcode';
    $column[] = 'bundle';
    $column[] = 'enabled';
    $column[] = 'title';
    $column[] = 'description';
    $column[] = 'menu_name';
    $column[] = 'link';
    $column[] = 'external';
    $column[] = 'rediscover';
    $column[] = 'weight';
    $column[] = 'expanded';
    $column[] = 'parent';
    $column[] = 'changed';
    $column[] = 'default_langcode';

    fputcsv($file, $column);

    foreach ($fields as $menu) {
      $menuLinkIds = \Drupal::entityQuery('menu_link_content')
        ->condition('menu_name', $menu)
        ->execute();

      $menuLinks = MenuLinkContent::loadMultiple($menuLinkIds);

      foreach ($menuLinks as $link) {
        $uri = $link->get('link');
        $row = [];
        $row[] = $link->get('id')->value;
        $row[] = $link->get('uuid')->value;
        $row[] = $link->get('langcode')->value;
        $row[] = $link->get('bundle')->value;
        $row[] = $link->get('enabled')->value;
        $row[] = $link->get('title')->value;
        $row[] = $link->get('description')->value;
        $row[] = $link->get('menu_name')->value;
        $row[] = serialize($link->get('link')->getValue());
        $row[] = $link->get('external')->value;
        $row[] = $link->get('rediscover')->value;
        $row[] = $link->get('weight')->value;
        $row[] = $link->get('expanded')->value;
        $row[] = $link->get('parent')->value;
        $row[] = $link->get('changed')->value;
        $row[] = $link->get('default_langcode')->value;

        fputcsv($file, $row);
      }
    }

    fclose($file);

    return $pathRelative;
  }


  /**
   * Compress files.
   *
   * @param $filename
   * @param $content_type
   *
   * @return string
   */
  public function createTarballExportMedia($filename, $content_type) {
    $pathAbsolute = $this->folders['public_migrate']['absolute'] . "/$filename";
    $pathRelative = $this->folders['public_migrate']['relative'] . "/$filename";
    $mediaPath = $this->folders['export_media']['absolute'] . "/$content_type";

    file_unmanaged_delete($pathAbsolute);

    //$archiver = new ArchiveTar($pathAbsolute, 'gz');
    //$archiver->addModify(["$mediaPath"], "$content_type", "$mediaPath");

    $archiver = new \ZipArchive();
    $archiver->open($pathAbsolute, \ZipArchive::CREATE);
    $handle = opendir($mediaPath);
    while ($f = readdir($handle)) {
      if ($f != "." && $f != "..") {
        if (is_file($mediaPath . "/" . $f)) {
          $archiver->addFile($mediaPath . "/" . $f, $content_type . "/" . basename($f));
        }
      }
    }
    $archiver->close();

    file_unmanaged_delete_recursive($mediaPath);

    return $pathRelative;
  }

  /**
   * Import menu from csv file.
   */
  public static function importMenuFromCsv($row, &$context) {
    $data = $row['data'];

    \Drupal::logger('senapi_content')
      ->info('Menu import name: @menu_name  menu title: @title, menu uuid: @uuid', [
        '@menu_name' => $data['menu_name'],
        '@title' => $data['title'],
        '@uuid' => $data['uuid'],
      ]);

    $menuName = $data['menu_name'];
    $menu = \Drupal::entityTypeManager()
      ->getStorage('menu')
      ->load($menuName);

    if (empty($menu)) {
      \Drupal::entityTypeManager()
        ->getStorage('menu')
        ->create([
          'id' => $menuName,
          'label' => $menuName,
          'expanded' => TRUE,
        ])->save();
    }

    $menuLinkEntity = \Drupal::entityQuery('menu_link_content')
      ->condition('uuid', $data['uuid'])
      ->execute();

    if (!$menuLinkEntity) {
      $menuLinkEntity = MenuLinkContent::create([
        'uuid' => $data['uuid'],
        'langcode' => $data['langcode'],
        'enabled' => $data['enabled'],
        'title' => $data['title'],
        'description' => $data['description'],
        'menu_name' => $data['menu_name'],
        'link' => unserialize($data['link']),
        'external' => $data['external'],
        'rediscover' => $data['rediscover'],
        'weight' => $data['weight'],
        'expanded' => $data['expanded'],
        'parent' => $data['parent'],
        //'changed' => $data['changed'],
        //'default_langcode' => $data['default_langcode'],
      ]);

      $operationDetails = ' Importado exitosamente.';
    }
    else {
      $menuLinkEntity = MenuLinkContent::load(reset($menuLinkEntity));
      $operationDetails = ' Actualizado exitosamente.';
    }

    $menuLinkEntity->save();
    unset($menuLinkEntity);

    $context['message'] = t('Ejecutando lote "@id" @details', [
      '@id' => $data['title'],
      '@details' => $operationDetails,
    ]);
    $context['results'] = $row['result'];
  }


  /**
   * Import block from csv file.
   */
  public static function importBlockFromCsv($row, &$context) {
    $data = $row['data'];

    \Drupal::logger('senapi_content')
      ->info('Block import type: @type  block info: @info, block uuid: @uuid', [
        '@type' => $data['type'],
        '@info' => $data['info'],
        '@uuid' => $data['uuid'],
      ]);

    $block_content = \Drupal::entityTypeManager()->getStorage('block_content')
      ->create(
        [
          $data['id'] => [
            'uuid' => $data['uuid'],
            'info' => $data['info'],
            'type' => $data['type'],
            'body' => [
              'value' => $data['body_value'],
              'format' => $data['body_format'],
            ],
          ]
        ]
      );

    $block_content->save();
    //$this->storeCreatedContentUuids([$block_content->uuid() => 'block_content']);

    $operationDetails = ' Importación exitosamente.';

    $context['message'] = t('Ejecutando lote "@id" @details', [
      '@id' => $data['info'],
      '@details' => $operationDetails,
    ]);
    $context['results'] = $row['result'];
  }

  /**
   * Image node content type.
   *
   * @param $image_src
   * @param $image_alt
   * @param $image_title
   * @param $dir_extract
   * @param $setting
   *
   * @return array
   */
  public function uploadImageMedia($image_src, $image_alt, $image_title, $dir_extract, $setting) {

    $imageSrc = explode('|', $image_src);
    $imageAlt = explode('|', $image_alt);
    $imageTitle = explode('|', $image_title);

    $destination = @self::getUploadLocation($setting);

    \Drupal::logger('senapi_content')
      ->info('Image src @image_src, Image extract @dir_extract, Image destination @destination', [
        '@image_src' => $image_src,
        '@dir_extract' => $dir_extract,
        '@destination' => $destination,
      ]);

    if (!file_prepare_directory($destination, FILE_CREATE_DIRECTORY)) {
      throw new HttpException(500, 'La ruta del archivo de destino no se puede escribir.');
    }

    @chmod($destination, 0777);

    if (count($imageSrc)) {
      $media = [];
      foreach ($imageSrc as $srcKey => $srcValue) {
        $uriFile = NULL;
        foreach (file_scan_directory($dir_extract, "/^$srcValue$/") as $uri => $file) {
          $uriFile = $uri;
        }

        if (!empty($uriFile)) {
          $file = file_save_data(file_get_contents($uriFile), $destination . '/' . $srcValue, FILE_EXISTS_RENAME);
          $imageArray = [
            'target_id' => $file->id(),
            'alt' => $imageAlt[$srcKey],
            'title' => $imageTitle[$srcKey],
          ];

          if (!isset($imageAlt[$srcKey])) {
            $imageArray = array_merge($imageArray, ['alt' => $imageAlt[0]]);
          }

          if (!isset($imageTitle[$srcKey])) {
            $imageArray = array_merge($imageArray, ['alt' => $imageTitle[0]]);
          }

          $media[] = $imageArray;
        }
      }
      return $media;
    }
    return [];
  }

  /**
   * File node content type.
   *
   * @param $file_src
   * @param $file_description
   * @param $dir_extract
   * @param $setting
   *
   * @return array
   */
  public function uploadFileMedia($file_src, $file_description, $dir_extract, $setting) {
    $fileSrc = explode('|', $file_src);
    $fileDescription = explode('|', $file_description);

    $destination = @self::getUploadLocation($setting);

    if (!file_prepare_directory($destination, FILE_CREATE_DIRECTORY)) {
      throw new HttpException(500, 'La ruta del archivo de destino no se puede escribir.');
    }

    @chmod($destination, 0777);

    \Drupal::logger('senapi_content')
      ->info('File src @file_src, Image extract @dir_extract, Image destination @destination', [
        '@file_src' => $file_src,
        '@dir_extract' => $dir_extract,
        '@destination' => $destination,
      ]);
    if (count($fileSrc)) {
      $media = [];
      foreach ($fileSrc as $srcKey => $srcValue) {
        $uriFile = NULL;
        foreach (file_scan_directory($dir_extract, "/^$srcValue$/") as $uri => $file) {
          $uriFile = $uri;
        }

        if (!empty($uriFile)) {
          $file = file_save_data(file_get_contents($uriFile), $destination . '/' . $srcValue, FILE_EXISTS_RENAME);
          $fileArray = [
            'target_id' => $file->id(),
            'description' => $fileDescription[$srcKey],
          ];

          if (!isset($fileDescription[$srcKey])) {
            $fileArray = array_merge($fileArray, ['alt' => $fileDescription[0]]);
          }

          $media[] = $fileArray;
        }
      }
      return $media;
    }
    return [];
  }

  /**
   * File node content type custom name.
   *
   * @param $file_src
   * @param $file_description
   * @param $dir_extract
   * @param $setting
   * @param array $attr
   *
   * @return array
   */
  public function uploadFileMediaCustomName($file_src, $file_description, $dir_extract, $setting, array $attr) {
    $fileSrc = explode('|', $file_src);
    $fileDescription = explode('|', $file_description);

    $destination = @self::getUploadLocation($setting);

    if (!file_prepare_directory($destination, FILE_CREATE_DIRECTORY)) {
      throw new HttpException(500, 'La ruta del archivo de destino no se puede escribir.');
    }

    @chmod($destination, 0777);

    \Drupal::logger('senapi_content')
      ->info('File src @file_src, Image extract @dir_extract, Image destination @destination', [
        '@file_src' => $file_src,
        '@dir_extract' => $dir_extract,
        '@destination' => $destination,
      ]);

    if (count($fileSrc)) {
      $media = [];
      foreach ($fileSrc as $srcKey => $srcValue) {
        $uriFile = NULL;
        foreach (file_scan_directory($dir_extract, "/^$srcValue$/") as $uri => $file) {
          $uriFile = $uri;
        }

        if (!empty($uriFile)) {
          $file = file_save_data(file_get_contents($uriFile), $destination . '/' . $attr['name'] . $attr['number'] . '_' . $attr['date'] . '.' . $attr['extension'], FILE_EXISTS_RENAME);
          $fileArray = [
            'target_id' => $file->id(),
            'description' => $fileDescription[$srcKey],
          ];

          if (!isset($fileDescription[$srcKey])) {
            $fileArray = array_merge($fileArray, ['alt' => $fileDescription[0]]);
          }

          $media[] = $fileArray;
        }
      }
      return $media;
    }
    return [];
  }

  /**
   * Batch completed.
   */
  public static function importFromCsvFinishedCallback($success, $results, $operations) {
    $messenger = \Drupal::messenger();
    if ($success) {
      $messenger->addMessage(t('@count procesados.', ['@count' => $results]));
    }
    else {
      $messenger->addMessage(t('Finalizado con error.'));
    }
  }


  /**
   * Get Upload Location Directory.
   *
   * @param array $settings
   *
   * @return string
   */
  public function getUploadLocation(array $settings) {
    $destination = trim($settings['file_directory'], '/');

    $destination = PlainTextOutput::renderFromHtml($this->token->replace($destination, []));
    return $settings['uri_scheme'] . '://' . $destination;
  }


  /**
   * Get list of content types.
   *
   * @return array
   */
  public function getAllContentTypes() {
    $contentTypes = NodeType::loadMultiple();

    $contentTypesList = [];
    foreach ($contentTypes as $contentType) {
      $contentTypesList[$contentType->id()] = $contentType->label();
    }
    return $contentTypesList;
  }

  /**
   * Get Content Type Fields.
   *
   * @param string $entity_type
   * @param null $content_type
   *
   * @return array
   */
  public function getFields($content_type, $entity_type = 'node') {
    $fields = [];

    if (empty($content_type)) {
      return $fields;
    }

    foreach ($this->fieldManager->getFieldDefinitions($entity_type, $content_type) as $fieldDefinition) {
      if (!empty($fieldDefinition->getTargetBundle()) && in_array($fieldDefinition->getType(), [
          'image',
          'entity_reference',
          'file',
        ])) {
        $fields['name'][] = $fieldDefinition->getName();
        $fields['properties'][] = [
          'name' => $fieldDefinition->getName(),
          'type' => $fieldDefinition->getType(),
          'setting' => $fieldDefinition->getSettings(),
        ];
      }
      else {
        if (in_array($fieldDefinition->getType(), [
          'datetime',
          'list_string',
          'text_long',
        ])) {
          $fields['name'][] = $fieldDefinition->getName();
          $fields['properties'][] = [
            'name' => $fieldDefinition->getName(),
            'type' => $fieldDefinition->getType(),
            'setting' => $fieldDefinition->getSettings(),
          ];
        }
        else {
          $fields['name'][] = $fieldDefinition->getName();
          $fields['properties'][] = [
            'name' => $fieldDefinition->getName(),
            'type' => $fieldDefinition->getType(),
            'setting' => $fieldDefinition->getSettings(),
          ];
        }
      }
    }

    return $fields;
  }

  /**
   * Get Content Type Fields.
   *
   * @param $bundle
   * @param $entity_type_id
   *
   * @return array
   */
  public function getAllItemFields($bundle, $entity_type_id) {
    $bundleFields = [];

    foreach ($this->fieldManager->getFieldDefinitions($entity_type_id, $bundle) as $field_name => $field_definition) {
      if (!empty($field_definition->getTargetBundle()) && in_array($field_definition->getType(), [
          'image',
          'entity_reference',
          'file',
        ])) {

        $bundleFields[$field_name] = $field_definition->getLabel() . ' - ' . $field_definition->getName() . "(" . $field_definition->getType() . ")";

      }
      else {
        if (in_array($field_definition->getType(), [
          'datetime',
          'list_string',
          'text_long',
        ])) {
          $bundleFields[$field_name] = $field_definition->getLabel() . ' - ' . $field_definition->getName() . "(" . $field_definition->getType() . ")";
        }
        else {
          if (!in_array($field_name, [
            'menu_link',
            'uuid',
            'uid',
            'nid',
            'vid',
            'langcode',
            'title',
            'type',
            'created',
            'changed',
            'promote',
            'sticky',
            'default_langcode',
            //'path',
            'comment',
            'revision_log',
            'revision_uid',
            'revision_timestamp',
            'revision_langcode',
            'revision_default',
            'revision_translation_affected',
          ])) {
            $bundleFields[$field_name] = $field_definition->getLabel() . ' - ' . $field_definition->getName() . "(" . $field_definition->getType() . ")";
          }
        }
      }
    }

    return $bundleFields;
  }
  /**
   * Get reference of taxonomy.
   *
   * @param $voc
   * @param $terms
   *
   * @return array
   */
  public function getTermReference($voc, $terms) {
    $vocName = strtolower($voc);
    $vid = preg_replace('@[^a-z0-9_]+@', '_', $vocName);
    $vocabularies = Vocabulary::loadMultiple();


    if (!isset($vocabularies[$vid])) {
      // Create Vocabulary
      @self::createVoc($terms, $vid);
    }

    $termArray = array_map('trim', explode('|', $terms));

    $termIds = [];

    foreach ($termArray as $term) {
      $termId = @self::getTermId($term, $vid);
      if (empty($termId)) {
        $termId = @self::createTerm($voc, $term, $vid);
      }
      $termIds[]['target_id'] = $termId;
    }
    return $termIds;
  }

  /**
   * Create terms if it is not available.
   *
   * @param $vid
   * @param $voc
   */
  public function createVoc($vid, $voc) {
    $vocabulary = Vocabulary::create([
      'vid' => $vid,
      'machine_name' => $vid,
      'name' => $voc,
    ]);

    $vocabulary->save();
  }

  /**
   * Get valid TermId.
   *
   * @param $term
   * @param $vid
   *
   * @return int|string|null
   */
  public function getTermId($term, $vid) {
    $query = \Drupal::entityQuery('taxonomy_term');
    $query->condition('vid', $vid);
    $query->condition('name', $term);

    $termResult = $query->execute();
    return key($termResult);
  }

  /**
   * Create terms if it is not available.
   *
   * @param $voc
   * @param $term
   * @param $vid
   *
   * @return int|string|null
   */
  public function createTerm($voc, $term, $vid) {
    Term::create([
      'parent' => [$voc],
      'name' => $term,
      'vid' => $vid,
    ])->save();

    $termId = @self::getTermId($term, $vid);
    return $termId;
  }

  /**
   * Get row of CSV file.
   *
   * @param resource $handle
   *   Resource handler.
   *
   * @return array
   *   Array of csv row.
   */
  public function getCsvData($handle) {
    return fgetcsv($handle, 0, ',', '"');
  }

  /**
   * Get file handle from uri.
   *
   * @param string $uri
   *   URI of file.
   *
   * @return resource
   *   Resource handler.
   */
  public function getFileHandler($uri) {
    return fopen($this->fileSystem->realpath($uri), "r");
  }

  /**
   * Remove dir content type
   */
  public function rmdir($uri) {
    file_unmanaged_delete_recursive($this->fileSystem->realpath($uri));
  }

  /**
   * Get an appropriate archiver class for the file.
   *
   * @param string $file
   *   The file path.
   */
  public function getArchiver($file) {
    $extension = strstr(pathinfo($file)['basename'], '.');
    switch ($extension) {
      case '.tar.gz':
      case '.tar':
        $this->archiver = new \PharData($file);
        break;

      case '.zip':
        $this->archiver = new \ZipArchive($file);
        $this->archiver->open($file);
      default:
        break;
    }
    return $this->archiver;
  }
}