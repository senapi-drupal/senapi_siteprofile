<?php


namespace Drupal\senapi_content\Form;


use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TermExportForm extends FormBase {
  /**
   * Set a var to make stepthrough form.
   *
   * @var step
   */
  protected $step = 1;

  /**
   * Set a var for export values.
   *
   * @var getExport
   */
  protected $getExport = '';

    /**
   * A instance of the EntityTypeManagerInterface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'term_export_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    switch ($this->step) {
      case 1:
        $form['vocabulary'] = [
          '#type' => 'select',
          '#title' => $this->t('Taxonomy'),
          '#options' => taxonomy_vocabulary_get_names(),
        ];
        $form['include_ids'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Include Term Ids in export.'),
        ];
        $form['include_headers'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Include Term Headers in export.'),
        ];
        $form['include_additional_fields'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Include extra fields in export.'),
          '#description' => $this->t('Note that fields are stringified using <a href="http://www.php.net/http_build_query">http_build_query</a>'),
        ];
        $form['submit'] = [
          '#type' => 'submit',
          '#value' => $this->t('Export'),

        ];
        break;

      case 2:
        $form['input'] = [
          '#type' => 'textarea',
          '#title' => $this->t('CSV Data'),
          '#description' => $this->t('The formatted term data'),
          '#value' => $this->getExport,
        ];
        break;
    }

    return $form;
  }

   /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->step++;
    $vocabulary = $form_state->getValue('vocabulary');
    $includeIds = $form_state->getValue('include_ids');
    $includeHeaders = $form_state->getValue('include_headers');
    $includeAdditionalFields = $form_state->getValue('include_additional_fields');

    $this->getExport = $this->execute($vocabulary, $includeIds, $includeHeaders, $includeAdditionalFields);
    $form_state->setRebuild();
  }

  public function execute($vocabulary, $include_ids, $include_headers, $include_fields) {
    $termRef = $this->entityTypeManager->getStorage('taxonomy_term');

    $terms = $termRef->loadTree($vocabulary);
    $fp = fopen('php://memory', 'rw');
    $standardTaxonomyFields = [
      'tid',
      'uuid',
      'langcode',
      'vid',
      'name',
      'status',
      'revision_id',
      'description__value',
      'description__format',
      'weight',
      'parent_name',
      'parent',
      'changed',
      'default_langcode',
      'path',
    ];

    $to_export = [];
    if ($include_headers) {
      $to_export = ['name', 'status', 'description__value', 'description__format', 'weight', 'parent_name'];
      if ($include_ids) {
        $to_export = array_merge(['tid', 'uuid'], $to_export);
        $to_export[] = 'parent_tid';
        array_splice($to_export, 4, 0, 'revision_id');
      }
      if ($include_fields) {
        //$to_export[] = 'fields';
      }
    }

    fputcsv($fp, $to_export);

    foreach ($terms as $term) {
      $parents = $termRef->loadParents($term->tid);
      $parent_names = '';
      $parent_ids = '';
      $to_export = [];
      if (!empty($parents)) {
        if (count($parents)  > 1) {
          foreach ($parents as $parent) {
            $parent_names .= $parent->getName().';';
            $parent_ids .= $parent->id().';';
          }
        }
        else if (count($parents) == 1){
          foreach ($parents as $parent) {
            $parent_names = $parent->getName();
            $parent_ids = $parent->id();
          }
        }
      }
      $to_export = [
        $term->name,
        $term->status,
        $term->description__value,
        $term->description__format,
        $term->weight,
        $parent_names,
      ];
      if ($include_ids) {
        $to_export = array_merge([$term->tid, $termRef->load($term->tid)->uuid()], $to_export);
        $to_export[] = $parent_ids;
        array_splice($to_export, 4, 0, $termRef->load($term->tid)->getRevisionId());
      }
      if ($include_fields) {
        $field_export = [];
        foreach ($termRef->load($term->tid)->getFields() as $field) {
          if (!in_array($field->getName(), $standardTaxonomyFields)) {
            foreach ($field->getValue() as $values) {
              foreach ($values as $value) {
                // Skip type ($key) here. More complicated, seems unnecessary.
                $field_export[$field->getName()][] = $value;
                if ($field->getName() == 'field_area_alias') {
                  $to_export[2] = $value;
                  $to_export[3] = 'restricted_html';
                }
              }
            }
          }
        }
        $fields = http_build_query($field_export);
        //$to_export[] = $fields;
      }
      fputcsv($fp, $to_export);
    }
    rewind($fp);
    while (!feof($fp)) {
      $this->getExport .= fread($fp, 8192);
    }
    fclose($fp);
    return $this->getExport;
  }
}