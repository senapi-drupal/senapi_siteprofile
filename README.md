## Drupal 8 profile SENAPI

## Requerimientos
- PHP 7.2
- Composer
- Drush

## Instalación

Instalar  proyecto base drupal composer:
 - `composer create-project drupal-composer/drupal-project:8.x-dev senapi --stability dev --no-interaction`

Ingresar a la carpeta de instalación `cd senapi/`, agregar los siguientes repositorios   
 - `composer config repositories.senapi_siteprofile vcs https://gitlab.com/senapi-drupal/senapi_siteprofile`
  
 - `composer config repositories.senapi_bootstrap vcs https://gitlab.com/senapi-drupal/senapi_bootstrap`
 
 - `composer config repositories.senapi_export_import vcs https://gitlab.com/senapi-drupal/senapi_export_import`
 
 - `composer config repositories.senapi_forms vcs https://gitlab.com/senapi-drupal/senapi_forms`


Habilitar patches en composer.json
```
{
    "extra": {
        "enable-patching": true
    }
}
  ```
    
Instalar perfil y dependencias:
 - `composer require senapi-drupal/senapi_siteprofile`


## Drupal Module
##### Custom 
    - senapi_export_import
    - senapi_forms

##### Contrib 
    - block_class 
    - pathauto
    - libraries
    
    Manejo de imagenes
    - slick (Slider, carrosel...)
    - slick_extras
    - slick_views
    - blazy (efecto imagen cargando)
    - blazy_ui
    - juicebox

    Desarrollo
    - admin_toolbar
    - devel

 
## Drupal Theme
##### Custom
    - senapi_bootstrap


##### Contrib
    - bootstrap_barrio

